﻿using System;
using System.Collections.Generic;
using Lab1.Contract;
using Lab1.Implementation;

namespace Lab1.Main
{
    public class Program
    {
        public static IList<IMundurowy> zbierz()
        {
            List<IMundurowy> mundurowi = new List<IMundurowy>();

            mundurowi.Add(new policjant());
            mundurowi.Add(new strazak());
            mundurowi.Add(new policjant());
            mundurowi.Add(new robot());

            mundurowi[0].dzialaj();
            mundurowi[1].dzialaj();
            mundurowi[2].dzialaj();
            mundurowi[3].dzialaj();

            return mundurowi;
        }

        public void end(List<IMundurowy> mundurowi)
        {
            mundurowi.ForEach(c => c.dzialaj());
        }

        static void Main(string[] args)
        {
        }
    }
}
