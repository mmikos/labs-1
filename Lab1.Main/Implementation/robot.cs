﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1.Contract;

namespace Lab1.Implementation
{
    class robot : Irobot, Ipolicjant
    {
        public string dzialaj()
        {
            return "robot podjal akcje";
        }

        public string przerwa()
        {
            return "robot przerwa";
        }

        public string reperacja()
        {
            return "reperacja";
        }

        public string ladowanie()
        {
            return "ladowanie"; 
        }

        string IMundurowy.przerwa()
        {
            return "dziala jak mundurowy";
        }

        string Irobot.przerwa()
        {
            return "dziala jak robot";
        }

        string Ipolicjant.przerwa()
        {
            return "dziala jak policjant";
        }

    }
}
