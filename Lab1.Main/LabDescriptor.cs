﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab1.Contract;
using Lab1.Implementation;

namespace Lab1.Main
{
    public struct LabDescriptor
    {

        #region P1

        public static Type IBase = typeof(IMundurowy);

        public static Type ISub1 = typeof(Ipolicjant);
        public static Type Impl1 = typeof(policjant);

        public static Type ISub2 = typeof(Istrazak);
        public static Type Impl2 = typeof(strazak);


        public static string baseMethod = "dzialaj";
        public static object[] baseMethodParams = new object[] { };

        public static string sub1Method = "dzialaj";
        public static object[] sub1MethodParams = new object[] { };

        public static string sub2Method = "dzialaj";
        public static object[] sub2MethodParams = new object[] { };

        #endregion

        #region P2

        public static string collectionFactoryMethod = "zbierz";
        public static string collectionConsumerMethod = "end";

        #endregion

        #region P3

        public static Type IOther = typeof(Irobot);
        public static Type Impl3 = typeof(robot);

        public static string otherCommonMethod = "przerwa";
        public static object[] otherCommonMethodParams = new object[] { };

        #endregion

    }
}
